from invoke import Collection, Program
from builder import tasks

program = Program(
    name="errant:tools.builder",
    binary='bld',
    version='dev',
    namespace=Collection.from_module(tasks)
    )
