from setuptools import setup

setup(
    name='errant-tools',
    version='dev',
    packages=['builder'],
    install_requires=['invoke'],
    entry_points={
        'console_scripts': ['bld = builder.main:program.run']
    }
)
